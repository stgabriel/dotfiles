"set nocompatible              " be iMproved, required
filetype off                  " required

" Specify a directory for plugins 
call plug#begin('~/.vim/plugged')
 
" Any valid git URL is allowed for plugin
"Plug 'valid git URL'
 
" Shorthand notation for plugin
"Plug 'foo/bar'
 
" Initialize plugin system



" this is the color scheme pywal thing.
Plug 'dylanaraps/wal.vim'

Plug 'tpope/vim-markdown'


" This makes all the double quotes work nicely
"
Plug 'tpope/vim-speeddating'
Plug 'kana/vim-textobj-user'
Plug 'preservim/vim-textobj-quote'
augroup textobj_quote
   autocmd!
   autocmd FileType markdown call textobj#quote#init()
   autocmd FileType textile call textobj#quote#init()
   autocmd FileType org call textobj#quote#init({'replace': 1})
   autocmd FileType text call textobj#quote#init({'educate': 0})
augroup END





" vim-pencil
"
Plug 'preservim/vim-pencil'

augroup pencil
      autocmd!
      autocmd FileType markdown,mkd,org call pencil#init()
      autocmd FileType text         call pencil#init()
augroup END



" Distraction free plugin
Plug 'junegunn/goyo.vim'
" Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
" Plug 'junegunn/goyo.vim', { 'for': '.org'}
" augroup goyo
"    autocmd!
"    autocmd FileType org call goyo#init()
" augroup END



" That focus effect
Plug 'junegunn/limelight.vim'

" org-mode
"
Plug 'jceb/vim-orgmode'

" this plugin for writers
Plug 'reedes/vim-pencil'

" fzf is a plugin for fuzzy finding
Plug 'junegunn/fzf.vim'

"
Plug 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub


" Plug 'git://git.wincent.com/command-t.git'


" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

Plug 'vimwiki/vimwiki' 


" nerdtree mapped to CNTL-O (see bottom of this file)
Plug 'scrooloose/nerdtree'


call plug#end()





filetype plugin indent on    " required

" for vimwiki these need to be on
" To ignore plugin indent changes, instead use:
filetype plugin on



"
" no user should have to live without.

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" this does wordwrap
set wrap
" 
set linebreak
set nolist  " list disables linebreak
set textwidth=0
set wrapmargin=0
set formatoptions-=t


" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.

" Enable syntax highlighting
syntax on

set wildmenu
set hidden

" Note that not everyone likes working this way (with the hidden option).
" Alternatives include using tabs or split windows instead of re-using the same
" window as mentioned above, and/or either of the following options:
" set confirm
" set autowriteall

" Show partial commands in the last line of the screen
set showcmd

" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch

" Modelines have historically been a source of security vulnerabilities. As
" such, it may be a good idea to disable them and use the securemodelines
" script, <http://www.vim.org/scripts/script.php?script_id=1876>.
" set nomodeline


"------------------------------------------------------------
" Usability options {{{1
"
" These are options that users frequently set in their .vimrc. Some of them
" change Vim's behaviour in ways which deviate from the true Vi way, but
" which are considered to add usability. Which, if any, of these options to
" use is very much a personal preference, but they are harmless.

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Always display the status line, even if only one window is displayed
set laststatus=2

" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm

" Use visual bell instead of beeping when doing something wrong
set visualbell

" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=

" Enable use of the mouse for all modes
set mouse=a

" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2

" Display line numbers on the left
set number

" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200

" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>

" added by kris
" re-index diary everytime a diary-entry is edited
"autocmd BuffWritePost ~/sync/diary !~/sync/scripts/reIndexDiary.sh 

"------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab

" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
"set tabstop=4


"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>

" added by kris: Turns on Goyo and Limelight with CNTL-L, obviously
map <C-l> :Goyo<CR>:Limelight<CR>

" map <C-k> <plug>ReplaceWithCurly<CR>
" map <silent> <leader>qs <plug>ReplaceWithCurly
"------------------------------------------------------------

" folding stuff
" added by kris
"set nocompatible
"  if has("autocmd")
"  filetype plugin indent on
"endif
" endif


"function! MarkdownLevel()
"    if getline(v:lnum) =~ '^# .*$'
"        return ">1"
"    endif
"    if getline(v:lnum) =~ '^## .*$'
"        return ">2"
"    endif
"    if getline(v:lnum) =~ '^### .*$'
"        return ">3"
"    endif
"    if getline(v:lnum) =~ '^#### .*$'
"        return ">4"
"    endif

"endfunction
"au BufEnter *.md setlocal foldexpr=MarkdownLevel()  
"au BufEnter *.md setlocal foldmethod=expr  


" added by kris - Wal plugin
" Using plug

colorscheme wal

" makes sure vim knows .md files are markdown not modula something
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

set spell spelllang=en_gb

" wordcount added by kris
"
 function! WordCount()
  let s:old_status = v:statusmsg
  let position = getpos(".")
  exe ":silent normal g\<C-g>"
  let stat = v:statusmsg
  let s:word_count = 0
  if stat != '--No lines in buffer--'
    let s:word_count = str2nr(split(v:statusmsg)[11])
    let v:statusmsg = s:old_status
  end
  call setpos('.', position)
  return s:word_count
endfunction

set statusline=   " clear the statusline for when vimrc is reloaded
set statusline+=%-3.3n\                      " buffer number
set statusline+=%f\                          " file name
set statusline+=%h%m%r%w                     " flags
set statusline+=[%{strlen(&ft)?&ft:'none'},  " filetype
set statusline+=%{strlen(&fenc)?&fenc:&enc}, " encoding
set statusline+=%{&fileformat}]              " file format
set statusline+=%10.10{WordCount()}w\   " wordcount
set statusline+=%=                           " right align
set statusline+=%{synIDattr(synID(line('.'),col('.'),1),'name')}\  " highlight
"set statusline+=%b,0x%-8B\                  " current char
set statusline+=%-14.(%l,%c%V%)\ %<%P        " offset
set noruler         "
set laststatus=2    " show statusline

" nerdtree map CNTL+o stuff
map <C-o> :NERDTreeToggle<CR>

" vimwiki/vimwiki
let g:vimwiki_list = [{'path': '~/sync/', 'syntax': 'markdown', 'ext': '.md'}] 
