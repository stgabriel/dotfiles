
# troubleshooting!

# mimeapps list.

on Arch, the file mimeapps.list should exist in ./config/mimeapps.list. Just symlink the file here to that location


on lenovo 720s:

"Sometimes upon reboot, rfkill may block wireless communications. To unblock, run : rfkill unblock all. "

if you have any problems with the wifi, do this:
https://bigi.dev/blog/2018-06-12-installing-antergos-linux-on-a-lenovo-ideapad-720-s-amd-ryzen-dual-booting-with-windows-10/


install:
xf86-video-amdgpu
amdgpu-pro-libgl from aur): 

add 'pci=noaer idle=nomwait' to kernel options


# good music player
musikcube  

# youtube-music
an electron GUI that lets you download music from youtube. but i had troubles with the most recent version in AUR, so i installed from source in the code dir. it puts a binary in the dist/ dir and that works fine.


### display drivers
### note: do NOT install amdvlk or lib32-amdvlk, according to:
### https://www.reddit.com/r/archlinux/comments/12ek36s/installing_arch_with_amd/
```

sudo pacman -S vulkan-radeon
sudo pacman -S mesa
sudo pacman -S libva-mesa-driver
sudo pacman -S mesa-vdpau
sudo pacman -S lib32-mesa
sudo pacman -S xf86-video-amdgpu
sudo pacman -S lib32-vulkan-radeon
sudo pacman -S libva-mesa-driver
sudo pacman -S lib32-libva-mesa-driver
sudo pacman -S mesa-vdpau
sudo pacman -S lib32-mesa-vdpau
```

fonts
```
yay ttf-ms-fonts
yay ttf-hack-nerd
yay ttf-dejavu-nerd

```

### nvidia monitor
wasnt turning back on, after suspend/sleep. So i activated these to see if it fixes.

```
sudo systemctl enable nvidia-suspend.service
sudo systemctl enable nvidia-hibernate.service
sudo systemctl enable nvidia-resume.service
```




Apple keyboard?
switch the FN and CNTL key permanently.


for now:
# echo "1" > /sys/module/hid_apple/parameters/swap_fn_leftctrl

permanent change:

create a file:
/etc/modprobe.d/hid_apple.conf
and add:
options hid_apple swap_fn_leftctrl=1

src: https://wiki.archlinux.org/title/Apple_Keyboard


# dotfiles

About sway: first make sure you're using the right framerate
```
swaymsg -t get_outputs
```

And check the framerate is set correctly (hz) at the top result.

You set the framerate and resolution in sway/config. Edit and look for the outputs line

```
output DP-1 mode 2560x1440@143.972Hz position 0,0

```
 
It's in a file called config.desktop. Use a symlink to point to that file in that dir, name the symlink 'config'.

## Trim on a ssd
```
sudo systemctl enable --now fstrim.timer 
```
## to install
openssh
polkit
dhcpcd
networkmanager
git
pamixer
alsa-utils
pulseaudio 

sway
# and or
hyprland


swayidle
waybar

# if hyprland installed, get the following
waybar-hyprland-git
# (or whatever seems good. waybar at present needs a patch to work with hyprland)

alacritty
python-pywal
git
eza # to alias 'ls'

# quickwall is awesome; it grabs the latest wallpaper from unsplash and sets it to the background with nitrogen

```
yay quickwall
yay nitrogen
```

thefuck
swaybg
# ranger
# nnn

ranger
(install w3m and imlib2, to get photo previews)

or

lf (or lf-bin) a ranger, written in golang

fuzzel # rofi replacement

wlogout
swaylock
grim * used by our swaylock script

mako
libnotify # gives you notify-send, which you can use to send messages to your desktop





i3
i3lock-colors

firefox
unzip
sshpass
lftp
lm_sensorsi # i think?
mpv
rsync
wget




# IMPORTANT SSH-related

copy across your ssh keys from tresorit and

chmod 600 ~/.ssh/marmelade
chmod 644 ~/.ssh/marmelade.pub
eval $(ssh-agent)
ssh-add ~/.ssh/marmelade

You could add to bash
```
if [ -z "$SSH_AUTH_SOCK" ]; then
    eval $(ssh-agent -s)
fi
```
or, more robustly,
systemctl --user enable ssh-agent
systemctl --user start ssh-agent


# for increasing/decreasing screen brightness

# android  > https://www.linuxfordevices.com/tutorials/linux/connect-android-to-arch-via-usb

brillo
polkitd

(The first lets you do increase/decrease screen brightness. the latter letters you make the changes not as root. manual page here:
https://gitlab.com/cameronnemo/brillo/-/blob/master/doc/man/brillo.1.md
)
examples:

> pkexec brillo -A 5
> pkexec brillo -U 5

There are scripts that run these commands in sway/scripts/bright

# emacs

is started in daemon mode, in .config/sway/conf/5_autostart

I wrote a function in .bashrc that effectively aliases emacs for emacsclient, and will take an argument file to load, and fail gracefully if one isnt received. More info about emacs in daemon is found here
https://wiki.archlinux.org/title/Emacs

# spellcheck

yay aspell
yay ispell * < required
yay hunspell
yay enchant
yay nuspell


# DICTIONARY




> yay -S sdcv    # lets you interact with stardict files.

dictionary files go under ~/.stardict/dic

> ln -s ~/code/dotfiles/websters.1913.dictionary ~/.stardict

if you want to put it anywhere else, you have to run this instead

> sdcv --data-dir ~/.config/dictionary
> sdcv --data-dir ~/.config/dictionary [word]


For more info:
http://mbork.pl/2017-01-14_I'm_now_using_the_right_dictionary
 https://luxagraf.net/src/how-use-websters-1913-dictionary-linux-edition


# fonts
cascadia-code
nerd-fonts-cascadia-code
nerd-fonts-ubuntu-mono
nerd-fonts-git # icons
adobe-source-sans-pro-fonts
ttf-droid
ttf-dejavu
ttf-ubuntu-font-family


#

pacman-contrib
systemctl enable paccache.timer # this cleans up your pacman cache once a week

# https://git.sr.ht/~kennylevinsen/greetd
# specifically install tuigreet


#protonvpn!
protonvpn-gui
protonvpn-cli
(these dependencies are practically secret, but most be installed!)
network-manager-applet
gnome-keyring


## hyprland
pacman -S hyprland
sudo pacman -S mako

yay wlogout
 yay wofi
 yay waybar
 yay swaymsg
  

(to run, 'hypr')


## the below is no longer used.

My config files for i3, openbox etc.

dunst

# pywal
in i3, CNTL+B and CNTL+SHIFT+B run scripts pywalDark.sh and pywalLight.sh

a dependency of those scripts is 'hsetroot', which isn't xsetroot. picom and xsetroot don't work well together.


# emacs pywal theme

copy or link ~/.emacs.d/pywal-theme.el   to -->  ~/.config/wal/templates/pywal-theme.el

then:

ln -s ~/.config/wal/pywal-theme.el ~/.emacs.d/themes/pywal-theme.el

Note: using C-c P will reload the theme in Emacs.



# important aps to install

install 'exa' (pacman -S exa). it's like 'ls' but better.
I have it aliased in bash

aconfmgr is a way of tracking installed apps.
https://github.com/CyberShadow/aconfmgr/

# inspirational

## all the polybar themes one could want
https://github.com/adi1090x/polybar-themes

## this boot iso project has good defaults
https://github.com/adi1090x/CustomArch/tree/version-2.0

# set some defaults


xdg-mime default thunar.desktop inode/directory

xdg-settings set default-web-browser firefox


# yay related.
go to yay dir, type

> makepkg -si


#now install with yay
fzf
hsetroot
polybar
cascadia-code
fuzzel
<<<<<<< HEAD


# wine
# these are basically for Diablo3. I found them here
# https://github.com/lutris/docs/blob/master/WineDependencies.md

sudo pacman -S --needed wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls \
mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error \
lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo \
sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \
ncurses lib32-ncurses ocl-icd lib32-ocl-icd libxslt lib32-libxslt libva lib32-libva gtk3 \
lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader

=======
>>>>>>> e04ac27 (.)


# wine
# these are basically for Diablo3. I found them here
# https://github.com/lutris/docs/blob/master/WineDependencies.md

sudo pacman -S --needed wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls \
mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error \
lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo \
sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \
ncurses lib32-ncurses ocl-icd lib32-ocl-icd libxslt lib32-libxslt libva lib32-libva gtk3 \
lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader


# EMAIL
protonmail-bridge-nogui
pass # ie. pacman -S pass

# initialize pass
pass init "zx2c4 Password Storage Key"

# now,
protonmail-bridge

# this is self-explanatory; add an account, etc. using the menu

# install isync

sudo pacman -S isync

# configure isync, default location is: ~/.mbsyncrc
```
IMAPAccount protonmail
Host 127.0.0.1
Port 1143
User stgabriel@protonmail.com
Pass <pw from protonmail-bridge>
#PassCmd "pass Email/example.com"
SSLType None
AuthMechs LOGIN

IMAPStore protonmail-remote
Account protonmail
UseNameSpace yes

MaildirStore protonmail-local
SubFolders Verbatim
Path ~/mail/protonmail/
Inbox ~/mail/protonmail/Inbox
Flatten .

Channel protonmail
Far :protonmail-remote:
Near :protonmail-local:
Create Both
Expunge Both
SyncState *
Sync All

Group protonmail
Channel protonmail-default
```
# create mail dir
mkdir ~/mail/protonmail


# to sync:
mbsync -a

# Using notmuch?
# configure notmuch
notmuch setup

# then, dont forget this step

notmuch new

# using mu4e

yay mu (hard to find, try mu4e, but the package is called 'mu').

# when install, initialise with

mu init -m ~/mail --my-address kris@stgabriel.io

mu index



