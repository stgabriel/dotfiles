#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls='exa -bghHliS --git-ignore'
#alias ls='exa -l'
alias ls='ls -al --ignore-backups --color'
#alias ls='eza -al --ignore-glob=*~'

PS1='[\u@\h \W]\$ '

HOME=/home/kris


# golang path
export PATH="$PATH:$(go env GOBIN):$(go env GOPATH)/bin"

#npm path
export PATH=~/.npm-global/bin:$PATH

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
PATH="$HOME/.local/bin:$PATH"
fi

PATH=$PATH:~/bin:~/.cargo/bin:~/.local/share/tresorit

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)


# Add this line to your .bashrc or a shell script.
. "${HOME}/.cache/wal/colors.sh"


## EMACS
# run as daemon
# 'emacs --daemon' is started by sway, in the autostart file.
# below is an alias function you wrote, kris:
# it takes the cmdline 'emacs' and takes the first parameter and loads it as emacsclient, because
# you have emacs being run as a daemon
# (in short you can't run 'emacs' now, it runs this.

# incidentally, now it does a check - if no argument, then initialise with an emtpy file. because otherwise the emacsclient throws errors if you just run it like 'emacsclient' with no file.


#  emacs(){

#   if [[ $# -eq 0 ]] ; then
#      #emacsclient -t
#      emacsclient -nw
#      # exit 0
#   else
#      #emacsclient "$1"
#      emacsclient -nw "$1"
#   fi
#  }
alias emacs='emacs -nw'
#alias emacs='emacsclient -nw'

# DICTIONARY (used by emacs chiefly)

# 1. install 'sdcv' (lets you use stardict format dictionarys)
# 2. make sure the websters dictionary (in code/dotfiles) is mapped to .config/dictionary.
# 2. this next function.

function l() {
  sdcv "$1" | less
  # sdcv --data-dir $HOME/.config/dictionary "$1" | less
}



# RANGER

r (){
  
  if [[ $# -eq 0 ]] ; then
  	ranger
  else
	ranger "$1" 
  fi
}

# adding The Fuck
# eval "$(thefuck --alias)"

#export TERM=xterm
#export TERM=alacritty
#export TERM=xterm-256color

 export VISUAL=emacs;
 export EDITOR=vim;

export TERM=xterm-256color


cd ~

# say you need to install something but want to do the password at the beginning instead of the end, use 'yays'.
alias yays='yay --sudoloop'

# mogrify!
# install it. this batch resizes images. 
# for more info: https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/



# python
# export WORKON_HOME=~/.virtualenvs
# source /usr/bin/virtualenvwrapper.sh

#   source /usr/share/nvm/init-nvm.sh


if [ -z "$SSH_AUTH_SOCK" ]; then
    eval $(ssh-agent -s)
fi
