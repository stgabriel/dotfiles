
#!/bin/bash

# uses betterlockscreen to screenlock
# https://github.com/pavanjadhaw/betterlockscreen#set-desktop-background-on-startup



# load pywal
source ~/.cache/wal/colors.sh # pywal scheme - update if needed

betterlockscreen -u $wallpaper

betterlockscreen -l
