#!/bin/bash

# Terminate already running bar instances
killall -q polybar
# this loads the colours from pywal (.Xresources is a symlink pointed at ~/.cache/wal/colors.Xresources )
xrdb ~/.Xresources
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
#polybar -r mybar
polybar

