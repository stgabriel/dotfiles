
(package-initialize)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))

;; uncomment below to update
;(package-refresh-contents)



;; emacs in daemon mode requires

(require 'package)

;; 'use-package' is a simpler package manager that installs packages
;; and sets them to autostart, i gather.
(unless (package-installed-p 'use-package)
    (package-refresh-contents)
      (package-install 'use-package))

(eval-when-compile
    (require 'use-package))


;;sable GUI elements. Why?
;; .. they take up screen-space and are unnecessary,    favor a minimal interface.
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

 ;; Disable startup message. Why?
 ;; .. less noise is better.
;;(defun display-startup-echo-area-message () (message ""))
(setq inhibit-startup-screen t) 
;; Visual bell. Why?
;; .. audible beeps are annoying.
(setq visible-bell 1);
;; we can set these packages to always open on start



;; add padding/margin to windows
;(set-window-margins (selected-window) 3 3)
;(setq scroll-margin 6)
;(setq header-line-format " ")
(set-frame-parameter (selected-frame) 'internal-border-width 30)

;; evil mode for vim bindings
;(use-package evil
;	     :ensure t)
;(evil-mode 1)

;; golang related by kris feb25,2023
(add-to-list 'load-path "~/.emacs.d/go-mode")
(autoload 'go-mode "go-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))





;; this is for typescript

  (use-package tide :ensure t)
  (use-package company :ensure t)
  (use-package flycheck :ensure t)

  (defun setup-tide-mode ()
    (interactive)
    (tide-setup)
    (flycheck-mode +1)
    (setq flycheck-check-syntax-automatically '(save mode-enabled))
    (eldoc-mode +1)
    (tide-hl-identifier-mode +1)
    ;; company is an optional dependency. You have to
    ;; install it separately via package-install
    ;; `M-x package-install [ret] company`
    (company-mode +1))

  ;; aligns annotation to the right hand side
  (setq company-tooltip-align-annotations t)

  ;; formats the buffer before saving
  (add-hook 'before-save-hook 'tide-format-before-save)

(add-hook 'typescript-mode-hook #'setup-tide-mode)

(add-to-list 'load-path "~/.emacs.d/web-mode")
 (require 'web-mode)

  (add-to-list 'auto-mode-alist '("\\.ts\\'" . web-mode))
  (add-hook 'web-mode-hook
            (lambda ()
              (when (string-equal "tsx" (file-name-extension buffer-file-name))
                (setup-tide-mode))))

  ;; enable typescript - tslint checker
  (flycheck-add-mode 'typescript-tslint 'web-mode)
;; end typescript









;; load org-mode
(use-package org
  :config

  
  ;(setq org-ellipsis " ▼")
  (setq org-ellipsis " ▾")


  (setq org-hide-emphasis-markers t))
  ;(custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
  
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-ellipsis ((t (:underline nil)))))
  
  (set-face-underline 'org-ellipsis nil)
; :ensure t)




;; distraction free editing
;; loads only in org-mode
(use-package olivetti
  :ensure t
  :config
  (add-hook 'org-mode-hook
	    (lambda ()
	      (olivetti-mode 1)
	      (setq olivetti-body-width 70))))



















(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(custom-safe-themes
   '("ec60daa8df31c416084da7d84fd0a65bfce7e5d10309910e792d3b14ff4f6418" default))
 '(package-selected-packages
   '(notmuch powerthesaurus flyspell-correct-ivy flyspell-correct focus org-sidebar org-roam org-journal journalctl-mode list-packages-ext use-package evil-tutor bbdb bbdb-vcard calfw calfw-ical easy-hugo magit olivetti xresources-theme ## org-evil org-mobile-sync evil)))


;;(setq org-default-notes-file (concat org-directory "~/projects/books/notes/bookmarks.org"))


