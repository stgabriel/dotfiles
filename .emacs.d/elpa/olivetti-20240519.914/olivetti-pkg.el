(define-package "olivetti" "20240519.914" "Minor mode for a nice writing environment"
  '((emacs "24.4"))
  :commit "683a13adc4197af632b35484d2b58bdb1d6c9b5e" :authors
  '(("Paul W. Rankin" . "hello@paulwrankin.com"))
  :maintainers
  '(("Paul W. Rankin" . "hello@paulwrankin.com"))
  :maintainer
  '("Paul W. Rankin" . "hello@paulwrankin.com")
  :keywords
  '("wp" "text")
  :url "https://github.com/rnkn/olivetti")
;; Local Variables:
;; no-byte-compile: t
;; End:
