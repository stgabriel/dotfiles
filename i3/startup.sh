#!/bin/sh

# this startup script runs xautolock, (install that!) so the screen wlll autosuspend
xautolock -time 5 -locker /home/kris/bin/lock -notify 20 -notifier 'xset dpms force off' &
xautolock -time 7 -locker "systemctl suspend" &
