#!/bin/sh
#!/bin/sh

BLANK='#00000000'
CLEAR='#ffffff22'
#DEFAULT='#00897bE6'
DEFAULT='#cccccc'
#TEXT='#00897bE6'
TEXT='#ffffff'
#WRONG='#880000bb'
WRONG='#00ffff'
#VERIFYING='#00564dE6'
VERIFYING='#00ffff'

i3lock \
    --radius=120  \
    --ring-width=20  \
--insidever-color=$CLEAR     \
--ringver-color=$VERIFYING   \
\
--insidewrong-color=$CLEAR   \
--ringwrong-color=$WRONG     \
\
--inside-color=$BLANK        \
--ring-color=$DEFAULT        \
--line-color=$BLANK          \
--separator-color=$DEFAULT   \
\
--verif-color=$TEXT          \
--wrong-color=$TEXT          \
--time-color=$TEXT           \
--date-color=$TEXT           \
--layout-color=$TEXT         \
--keyhl-color=$WRONG         \
--bshl-color=$WRONG          \
\
--screen 1                   \
--blur 9                     \
--clock                      \
--indicator                  \
--time-str="%H:%M:%S"        \
--date-str="%A, %Y-%m-%d"       \
--keylayout 1                \
