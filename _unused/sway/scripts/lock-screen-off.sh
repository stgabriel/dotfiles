#!/bin/bash
swayidle -w  \
	timeout 1 '~/.config/sway/lock.sh' \
	timeout 2 'swaymsg "output * dpms off"' \
	resume 'swaymsg "output * dpms on"; pkill -nx swayidle'

