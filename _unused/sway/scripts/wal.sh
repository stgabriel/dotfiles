# run wal
#wal -l -i ~/code/dotfiles/pictures/
wal -i ~/code/dotfiles/pictures
# now fetch its values
. "${HOME}/.cache/wal/colors.sh"

# now we could do:
# echo $wallpaper

# i use oguri because it lets you do
# animated gifs.
# install:
# yay oguri

# oguri is always running, so kill it.
killall oguri
# now start it again
oguri -c ~/.cache/wal/oguri-config 
# swaybg -m fill -i $wallpaper
