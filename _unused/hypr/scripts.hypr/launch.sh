#!/bin/bash


# Terminate already running bar instances

exec systemd-run --user$(killall -q waybar && pkill -SIGUSR1 '^waybar$')
sleep 1

# Wait until the waybar processes have been shut down

# while pgrep -x waybar >/dev/null; do sleep 1; done

# Launch main

exec systemd-run --user $(waybar -s /home/kris/.config/waybar/style.css -c /home/kris/.config/waybar/config.hypr)
#waybar -s /home/kris/code/dotfiles/waybar/style.css -c ~/.config/waybar/config.hypr

#sleep 4
#pkill -SIGUSR1 '^waybar$' 
#sleep 1
#waybar -c ~/.config/waybar/scripts.hypr/config.hypr
